import React, { Component } from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { withStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const styles = {

  gridItem: {
    padding: 8
  },

  inputField: {
    width: "400px",
    margin: "15px",
    fontSize: "18px"
  },
  textArea: {
    fontSize: "18px",
    width: "600px",
  },
  button: {
    margin: "15px",
    fontSize: "18px"
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      comment: "",
      date: this.getDate(),
      comments: []
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getComments() {
    fetch("http://localhost:3212/feedbacks",
      {
        method: "GET",
        headers: { 'Content-Type': 'application/json' },
        mode: 'cors'
      })
      .then(res => res.json())
      .then(data => this.setState({ comments: data.body }))
      .catch(function (error) {
        console.log(error);
      });
  }

  componentDidMount() {
    this.getComments()
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  getDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = String(today.getFullYear());


    return yyyy + "-" + mm + "-" + dd
  }

  handleSubmit() {
    if (this.state.name && this.state.email && this.state.comment) {
      let data = {
        name: this.state.name,
        email: this.state.email,
        date: this.state.date,
        comment: this.state.comment

      }

      fetch("http://localhost:3212/feedbacks",
        {
          method: "POST",
          headers: { 'Content-Type': 'application/json' },
          mode: 'cors',
          body: JSON.stringify(data),
        })
        .then(res => res.json())
        .then(function (response) {
          console.log("ok");
        })
        .catch(function (error) {
          console.log(error);
        });
      this.getComments()
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div style={{ backgroundColor: "#F2F2F2", minHeight: '100vh', margin: 0, overflow: "hidden" }} >
        <Grid
          container
          spacing={8}
          direction="row"
          style={{ padding: 10, margin: 10 }}
        >
          <Grid item xs={5}
            className={classes.gridItem}>
            <Paper>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
              >

                <Input placeholder="Vardas"
                  className={classes.inputField}
                  id="NameInputField"
                  onChange={this.handleChange("name")}
                />
                <Input placeholder="El. paštas"
                  className={classes.inputField}
                  onChange={this.handleChange("email")} />
                <TextField
                  id="date"
                  type="date"
                  defaultValue={this.state.date}
                  className={classes.inputField}
                  disabled
                />

                <TextField
                  variant="outlined"
                  multiline
                  rows="6"
                  placeholder="Komentaras"
                  className={classes.textArea}
                  onChange={this.handleChange("comment")}
                />
                <Button color="primary" className={classes.button} onClick={this.handleSubmit}>Pateikti</Button>
              </Grid >
            </Paper>
          </Grid>
          <Grid item xs={5} className={classes.gridItem}>
            <Typography color="primary" variant="h4" >
              Komentarai
        </Typography>
            {this.state.comments.map((c, index) =>
              <Card key={index} >
                <CardActionArea >
                  <CardContent>
                    <Typography color="primary" variant="h6" >
                      {c.name}
                    </Typography>
                    <Typography className={classes.pos} >
                      {c.date}
                    </Typography>
                    <Typography variant="body1">
                      {c.comment}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            )}
          </Grid>
        </Grid>
      </div>
    );
  }

}

export default withStyles(styles)(App);
